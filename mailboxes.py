#!/usr/bin/python3
import kopano
from kopano.errors import *
from MAPI.Tags import *

for user in kopano.users():
    try:
        logon = user.store.prop(PR_LAST_LOGON_TIME).value
    except:
        logon = "never"

    print("{},{},{}".format(user.prop(PR_EMAIL_ADDRESS_W).value, user.store.size if user.store else "", logon))
