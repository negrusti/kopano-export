import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders

def send(from_addr, to_addr, subject, body, attachment):
    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Subject'] = subject

    msg.attach(MIMEText(body))

    with open(attachment, 'rb') as file:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + attachment)
        msg.attach(part)

    try:
        with smtplib.SMTP("localhost", 25) as server:
            server.sendmail(from_addr, to_addr, msg.as_string())
        print("Email sent successfully!")
    except e as Exteption:
        print("Sending failed: ", str(e))


