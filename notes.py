#!/usr/bin/python3
import sys
import os
import kopano
from kopano.errors import NotFoundError
import csv
import json
import re
import zipfile
from send import send

def compress_txt_files_to_zip(directory_path, zip_path):
    # Check if the provided directory path exists
    if not os.path.exists(directory_path):
        print("Directory " + directory_path + " not found.")
        return

    # Overwrite the zip file if it exists
    if os.path.exists(zip_path):
        os.remove(zip_path)

    # Create a new zip file
    with zipfile.ZipFile(zip_path, 'w') as zip_file:
        for file in os.listdir(directory_path):
            if file.endswith('.txt'):
                file_path = os.path.join(directory_path, file)
                zip_file.write(file_path, os.path.basename(file_path))

def write_note(filepath, data):
    # split the filepath into directory and filename parts
    directory = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    # create the directory if it doesn't exist
    if not os.path.exists(directory):
        os.makedirs(directory)
    # create the file in the directory
    with open(os.path.join(directory, filename), 'w') as f:
        f.write(data)  # write an empty string to the file

def main():
    for user in kopano.Server().users():
        print("User: ", user.name)

        note_no = 0
        try:
            for folder in user.folders():
                try: 
                    if folder.container_class.startswith('IPF.StickyNote'):
                        for item in folder:
                            write_note('notes/' + user.name + '/' + str(note_no).zfill(4) + '.txt', item.text)
                            note_no = note_no + 1

                        if note_no:
                            compress_txt_files_to_zip('./notes/' + user.name, './notes/' + user.name + '/notes.zip')
                            send("test@example.com",
                                user.name,
                                "Transmission de vos notes en fichiers TXT",
                                "Transmission de vos notes en fichiers TXT",
                                'notes/' + user.name + '/notes.zip')

                except AttributeError:
                    pass

        except TypeError:
            pass

if __name__ == "__main__":
    main()
