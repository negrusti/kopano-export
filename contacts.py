#!/usr/bin/python3
import sys
import os
import kopano
from kopano.errors import NotFoundError
import csv
import json
import re
from send import send

def write_vcf(filepath, data):
    # split the filepath into directory and filename parts
    directory = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    # create the directory if it doesn't exist
    if not os.path.exists(directory):
        os.makedirs(directory)
    # create the file in the directory
    with open(os.path.join(directory, filename), 'w') as f:
        f.write(data)  # write an empty string to the file

def main():
    vcftypes = set()
    
    for user in kopano.Server().users():
        contacts = []
        print("User: ", user.name)
        contacts_folder = None
        try:
            for folder in user.folders():
                if folder.name == 'Contacts':
                    contacts_folder = folder
                    break
        except TypeError:
            break

        vcf_no = 0
        for item in contacts_folder:            
            if item.message_class == 'IPM.Contact':
                contact = {}
                emails = set()
                vcf = item.vcf().decode()
                write_vcf('contacts/' + user.name + '/' + str(vcf_no).zfill(4) + '.vcf', vcf)
                vcf_no = vcf_no + 1
                for line in vcf.splitlines():
                    vcftypes.add(line.split(':')[0])

                    if line.startswith('FN'):
                        contact['name'] = line.split(':')[1].strip()

                    elif line.startswith('ORG'):
                        contact['organization'] = line.split(':')[1].strip()

                    elif line.startswith('TEL;TYPE=MOBILE'):
                        contact['phone_mobile'] = line.split(':')[1].strip()

                    elif line.startswith('TEL;TYPE=HOME'):
                        contact['phone_home'] = line.split(':')[1].strip()

                    elif line.startswith('TEL;TYPE=MAIN;TYPE=FAX'):
                        contact['phone_fax'] = line.split(':')[1].strip()

                    elif line.startswith('TEL;TYPE=MAIN'):
                        contact['phone_main'] = line.split(':')[1].strip()

                    elif line.startswith('ADR;TYPE=HOME'):
                        contact['address_home'] = line.split(':')[1]
                        address_home_values = contact['address_home'].split(';')
                        address_home_keys = ["address_home_pobox",
                                "address_home_extaddr",
                                "address_home_straddr",
                                "address_home_city",
                                "address_home_region",
                                "address_home_code",
                                "address_home_country"]
                        address_home_data = dict(zip(address_home_keys, address_home_values))
                        contact.update(address_home_data)

                    elif line.startswith('ADR;TYPE=WORK'):
                        contact['address_work'] = line.split(':')[1]
                        address_work_values = contact['address_work'].split(';')
                        address_work_keys = ["address_work_pobox",
                                "address_work_extaddr",
                                "address_work_straddr",
                                "address_work_city",
                                "address_work_region",
                                "address_work_code",
                                "address_work_country"]
                        address_work_data = dict(zip(address_work_keys, address_work_values))
                        contact.update(address_work_data)

                    elif line.startswith('EMAIL;TYPE=INTERNET'):
                        emails.add(line.split(':')[1].strip())

                contact['email'] = ';'.join([str(m) for m in emails])
                contacts.append(contact)

        try:
            suggestions = json.loads(user.store.prop(0X6773001F).value, strict=False, encoding="utf-8")
        except:
            suggestions = None

        if suggestions:
            for recipient in suggestions['recipients']:
                contact = {}
                if re.search(r'u00[0-9a-f]{2}', recipient['display_name']):
                    name = re.sub(r'(u00[0-9a-f]{2})', r'\\\1', recipient['display_name']).encode("utf-8").decode("unicode_escape")
                else:
                    name = recipient['display_name']
                contact['name'] = name
                contact['email'] = recipient['smtp_address'] if recipient['smtp_address'] else recipient['email_address']
                try:
                    contact['domain'] = contact['email'].split('@')[1]
                    contacts.append(contact)
                except IndexError:
                    print('Invalid autocomplete entry: \n' + json.dumps(recipient, indent = 4))
                    continue 

        if len(contacts) > 0:
            with open('contacts/' + user.name + '.csv', 'w', newline='') as csvfile:
                csv_writer = csv.writer(csvfile)
                csv_writer.writerow(['Full Name',
                                     'Organization',
                                     'Email',
                                     'Main Phone',
                                     'Home Phone',
                                     'Mobile',
                                     'Work Address',
                                     'Work Street 1',
                                     'Work City',
                                     'Work State',
                                     'Work Postal Code',
                                     'Work Country',                                     
                                     'Home Address',
                                     'Home Street 1',
                                     'Home City',
                                     'Home State',
                                     'Home Postal Code',
                                     'Home Country',
                                     'Nickname'])
                for c in contacts:
                    csv_writer.writerow([c.get('name'),
                                         c.get('organization'),
                                         c.get('email'),
                                         c.get('phone_main'),
                                         c.get('phone_home'),
                                         c.get('phone_mobile'),
                                         c.get("address_work_extaddr"),
                                         c.get("address_work_straddr"),
                                         c.get("address_work_city"),
                                         c.get("address_work_region"),
                                         c.get("address_work_code"),
                                         c.get("address_work_country"),
                                         c.get("address_home_extaddr"),
                                         c.get("address_home_straddr"),
                                         c.get("address_home_city"),
                                         c.get("address_home_region"),
                                         c.get("address_home_code"),
                                         c.get("address_home_country"),
                                         c.get('domain')])

            send("from@example.com",
                "to@example.com",
                "Kopano contacts export",
                "Kopano contacts export",
                'contacts/' + user.name + '.csv')

if __name__ == "__main__":
    main()
